export const msalConfig = {
  auth: {
    clientId: "3ef254b5-2860-4b20-bb90-0dd74a32c963",
    authority: "https://login.microsoftonline.com/2bbe0344-ca53-45b2-b5a5-f1725139ad13/",
    redirectUri: `${window.location.href}`,
  },
  cache: {
    cacheLocation: "localStorage", // This configures where your cache will be stored
    storeAuthStateInCookie: false, // Set this to "true" if you are having issues on IE11 or Edge
  }
};
export const graphConfig = {
  graphMeEndpoint: "https://graph.microsoft.com/me"
};

// Add scopes here for ID token to be used at Microsoft identity platform endpoints.
export const loginRequest = {
 scopes: ["api://3ef254b5-2860-4b20-bb90-0dd74a32c963/.default"]
};

