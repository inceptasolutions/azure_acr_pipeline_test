/**
 * Enum for componentsType
 */
export enum ComponentsType {
  Temperature = "temperature",
  Pressure = "pressure",
  Humidity = "humidity",
  AirQuality = "airQuality",
}

/**
 * Enum for componentsReadingValue
 */
export enum ComponentsReadingValue {
  Temperature = "temperature",
  Pressure = "bp",
  Humidity = "humidity",
  AirQuality = "iaq",
}
/**
 * Interface for alarm data response that comes from alert API
 *
 * @interface AdminTableResponse
 */
export interface AdminTableResponse {
  deviceId: string;
  componentId: string;
  eventId: string;
  conditionComponentDirection: string;
  conditionComponentType: string;
  conditionComponentLimit: string;
  conditionComponentDuration: string;
  datetimeInserted: string;
  message: string;
  function: Function;
}

/**
 * Interface for function data that represents the reading data from alert API
 * This interface is used in AdminTableResponse interface
 *
 * @interface Function
 */
interface Function {
  [propertyName: string]: any;
}


/**
 * Interface for last reading data that comes from lastReading API
 *
 * @interface LastReadingResponse
 */
export interface LastReadingResponse {
  deviceId: string;
  function: lastReadingFunctions[];
  isConnected: boolean | null;
  location: string;
  siteId: string;
  type: string;
}

/**
 * Interface for last reading functions that is used in LastReadingResponse interface
 *
 * @interface lastReadingFunctions
 */
export interface lastReadingFunctions {
  componentId: string;
  componentName: string;
  componentValue: string;
  datetime: string;
  isToggle: string;
}

/**
 * Interface for device table response that comes from pageRefresh API
 *
 * @interface DeviceTableResponse
 */
export interface DeviceTableResponse {
  deviceId: string;
  isConnected: boolean;
  status: string;
  location: string;
  rule: any[];
  schedule: any[];
  siteId: string;
  type: string;
}

/**
 * Interface for schedule request format for the schedule API
 *
 * @interface ScheduleRequest
 */
export interface ScheduleRequest {
  deviceId: string;
  scheduleName: string;
  start: StartEnd;
  end: StartEnd;
  during: {
    weekdays: string[];
  };
}

/**
 * Interface for start and end time that is used in interface ScheduleRequest
 *
 * @interface StartEnd
 */
export interface StartEnd {
  action: {
    componentId: string;
    type: string;
    time: string;
  };
}

/**
 * enum for Days of the Week
 */
export enum DaysOfTheWeek {
  Monday = "monday",
  Tuesday = "tuesday",
  Wednesday = "wednesday",
  Thursday = "thursday",
  Friday = "friday",
  Saturday = "saturday",
  Sunday = "sunday",
}

/**
 * Interface for DayMap that represents two groups such as weekdays and weekends as string.
 * Day is another interface that includes the properties of a day
 *
 * @interface DayMap
 */
export interface DayMap {
  [propertyName: string]: Day[];
}

/**
 * Interface for Day that represents the keys of a day.
 * Day is used in another interface DayMap
 *
 * @interface Day
 */
export interface Day {
  title: string;
  startTime: string;
  endTime: string;
  toggle: boolean;
  isChanged: boolean;
}

/**
 * @type {object}
 *
 * Unit is used to create another object for rules component type, component id, unit scale
 */
export type Unit = {
  [process: string]: string;
};

/**
 * @type {object}
 *
 * RuleCreateRequest is for request body
 * that is sent through rules API while rule creating
 */
export type RuleCreateRequest = {
  deviceId: string;
  ruleName: string;
  condition: {
    type: string;
    componentId: string;
    min: number;
    max: number;
    limit: string;
    duration: string;
  };
  notifyEvent: boolean;
  action: {
    type: string;
  };
};

/**
 * @type {object}
 *
 * RuleDeleteRequest is for request body
 * that is sent through rules API while rule deleting
 */
export type RuleDeleteRequest = {
  deviceId: string;
  ruleName: string;
  condition: {
    type: string;
    componentId: string;
    min: string;
    max: string;
    duration: string;
  };
  notifyEvent: false;
  action: {
    type: string;
  };
};

/**
 * Interface for rule data that come from pageRefresh API
 *
 * @interface Rule
 */
export interface Rule {
  componentId: string;
  eventId: string;
  ruleId: string;
  ruleName: string;
  componentName: string;
  conditionComponentDirection: string;
  conditionComponentType: string;
  conditionComponentLimit: string;
  minLimit: string;
  maxLimit: string;
  conditionComponentDuration: string;
  notifyEvent: string;
  actionComponentType: string;
}

/**
 * Interface for rule table data
 * that represents to the UI on device details page
 *
 * @interface RuleForTable
 */
export interface RuleForTable {
  type: string;
  minLimit: string | null | undefined;
  maxLimit: string | null | undefined;
  duration: string;
  outletStatus: string;
}

/**
 * enum for components name
 */
 export enum Components {
    Temperature = "temperature",
    Pressure = "pressure-bp",
    Humidity = "humidity",
    AirQuality = "aq-iaq",
  }