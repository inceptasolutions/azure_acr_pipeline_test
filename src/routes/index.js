import { Switch, Route, Redirect } from "react-router-dom";
import Home from "../pages/Home";
import DeviceDetails from "../pages/DeviceDetails";
import Admin from "../pages/Admin";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/device-details/:id" component={DeviceDetails} />
      <Route exact path="/admin" component={Admin} />

      {/* Redirect to Home while any wrong pathname is tried */}
      <Route path="*" render={() => <Redirect to="/" />} />
    </Switch>
  );
};

export default Routes;
