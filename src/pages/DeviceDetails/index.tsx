/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ScheduleRule from "../../components/ScheduleRule";
import api from "../../services/http";
import DeviceInfo from "../../components/DeviceInfo";
import ManualOverride from "../../components/ManualOverride";
import { LastReadingResponse, Components } from "../../helpers/types";

const DeviceDetails = () => {
  document.title = "Device Details Page | SmartConnect";

  const { id } = useParams<{ id: string }>();

  const [device, setDevice] = useState<LastReadingResponse>();
  const [lastUpdateTime, setLastUpdateTime] = useState("");
  const [loaderForOverride, setLoaderForOverride] = useState<boolean>(false);

  useEffect(() => {
    getDeviceLastReading();
  }, []);

  /**
   * @function getDeviceLastReading
   * Function to call the lastReading API to get the device reading data
   * And find out the selected device data matching with device Id in the url
   * Update the device state and last update time state from local machine
   */
  const getDeviceLastReading = () => {
    api<LastReadingResponse[]>("get", "device/lastReading", "").then((data) => {
      const response = data.data;
      const finalDevice = response.find((entry) => entry.deviceId === id);
      setDevice(finalDevice);
      setLastUpdateTime(Date().toLocaleString());
    });
  };

  /**
   * @function rulesComponents
   * Get the list of components type of rules
   *
   * @returns {Array} Array of string that represents a list of rules components type
   */
  const rulesComponents = () => {
    let rulesComponentsArray: string[] = [];
    device?.function.forEach((entry) => {
      if (entry.componentName === Components.Temperature) {
        rulesComponentsArray.push("Temperature");
      }
      if (entry.componentName === Components.Humidity) {
        rulesComponentsArray.push("Humidity");
      }
      if (entry.componentName === Components.Pressure) {
        rulesComponentsArray.push("Pressure");
      }
      if (entry.componentName === Components.AirQuality) {
        rulesComponentsArray.push("Air Quality");
      }
    });
    return rulesComponentsArray;
  };

  /**
   * @function formatTime
   * Change the time format from default date string to the specific date format
   *
   * @param {string} time - date string as toLocaleString()
   * @returns {string} formatted date and time as 'Aug 12 2021 23:29:44'
   */
  const formatTime = (time: string): string => {
    const splittedTime = time.split(" ");
    return splittedTime.slice(1, 5).join(" ");
  };

  return (
    <div className="right">
      <div className="top">
        <h1>Device Details</h1>
      </div>

      {/* Device info area that is from lastReading API */}
      <DeviceInfo
        device={device}
        getLastReading={getDeviceLastReading}
        time={formatTime(lastUpdateTime)}
        loaderForOverride={loaderForOverride}
      />

      {/* Schedule or Rule data area */}
      <ScheduleRule deviceId={id} rulesComponents={rulesComponents()} />

      {/* Temporary Override section */}
      <ManualOverride
        device={device}
        getLastReading={getDeviceLastReading}
        setLoaderForOverride={setLoaderForOverride}
      />
    </div>
  );
};

export default DeviceDetails;
