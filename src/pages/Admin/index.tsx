import { useEffect, useState } from "react";
import refreshWhiteIcon from "../../assets/images/refresh-white.png";
import carbonNotificationIcon from "../../assets/images/carbon_notification.png";
import pressureMaxIcon from "../../assets/images/pressure-above-max.png";
import tempMinIcon from "../../assets/images/temperature-below-min.png";
import tempMaxIcon from "../../assets/images/temperature-above-max.png";
import humiMaxIcon from "../../assets/images/humidity-above-max.png";
import airMaxIcon from "../../assets/images/air-quality-above-max.png";
import SearchBar from "../../components/SearchBar";
import Table from "../../components/DeviceTable";
import api from "../../services/http";
import moment from "moment";
import { ColumnFilter } from "../../components/DeviceTable/ColumnFilter";
import Loader from "react-loader-spinner";
import { DatePicker } from "antd";
import {
  AdminTableResponse,
  ComponentsReadingValue,
  ComponentsType,
} from "../../helpers/types";


const dateTimeFormat = "YYYY-MM-DD HH:mm";

const Admin = () => {
  document.title = "Admin Page | SmartConnect";

  const [data, setData] = useState<AdminTableResponse[]>([]);
  const [searchValue, setSearchValue] = useState("");
  const [filteredData, setFilteredData] = useState<AdminTableResponse[]>([]);
  const [dateTimeRange, setDateTimeRange] = useState<string[]>([]);
  const [loaderSpinner, setLoaderSpinner] = useState<boolean>(true);

  /**
   * RangePicker - Object destructuring from datePicker
   * It is used for ranging date and time together
   */
  const { RangePicker } = DatePicker;

  /**
   * Columns for alarm table that represent as table heading on admin page
   */
  const columns = [
    {
      Header: "Device ID",
      accessor: "deviceId",
      Filter: ColumnFilter,
      width: 50,
    },
    {
      Header: "Date",
      accessor: (d: any) => {
        return moment(d.datetimeInserted).local().format("DD-MM-YYYY");
      },
      Filter: ColumnFilter,
      width: 70,
    },
    {
      Header: "Time",
      accessor: (t: any) => {
        return moment(t.datetimeInserted).local().format("hh:mm A");
      },
      Filter: ColumnFilter,
      width: 20,
    },
    {
      Header: "Reading",
      accessor: (props: any) => {
        return (
          <span className="reading text-not-uppercase">
            {" "}
            {handleReading(props)}{" "}
          </span>
        );
      },
      Filter: ColumnFilter,
      width: 10,
    },
    {
      Header: "Details",
      accessor: (props: any) => {
        return (
          <p>
            <img src={handleDetailsIcon(props)} alt="icon" /> {props.message}{" "}
          </p>
        );
      },
      Filter: ColumnFilter,
      width: 150,
    },
  ];

  /**
   * @function getComponentType
   * Format conditionComponentType that comes from alert API
   * Remove 'condition.' from every conditionComponentType string
   *
   * @param {any} props - props data is come from data table row
   * @returns {string} component type
   */
  const getComponentType = (props: any) => {
    if (props.conditionComponentType) {
      const str = props.conditionComponentType.split(".")[1];
      const type = str?.charAt(0).toLowerCase() + str?.slice(1);
      return type;
    }
  };

  /**
   * @function handleReading
   * Get reading data that represents on Reading column cell of the alarm table
   * Set different symbols such as °C, %, hPa according to the types
   *
   * @param {any} props - props data is come from data table row
   * @returns {string} reading value for the column cell
   */
  const handleReading = (props: any) => {
    const readingFunction = props.function;
    const componentId = props.componentId;
    let componentType;

    componentType = getComponentType(props);

    const getReadingValue = (cId: string, rValue: string) => {
      const tempValue = `${Math.ceil(
        parseInt(readingFunction[cId][rValue])
      ).toString()}`;

      return tempValue;
    };

    switch (componentType) {
      case ComponentsType.Temperature:
        return `${getReadingValue(
          componentId,
          ComponentsReadingValue.Temperature
        )}°C`;
      case ComponentsType.Humidity:
        return `${getReadingValue(
          componentId,
          ComponentsReadingValue.Humidity
        )}%`;
      case ComponentsType.Pressure:
        return `${getReadingValue(
          componentId,
          ComponentsReadingValue.Pressure
        )}hPa`;
      case ComponentsType.AirQuality:
        return `${getReadingValue(
          componentId,
          ComponentsReadingValue.AirQuality
        )}`;

      default:
        break;
    }
  };

  /**
   * @function handleDetailsIcon
   * Set different icons according to the conditionComponentDirection as exceed or subseed
   * That represent on Details column cell of the alarm table
   *
   * @param {any} props
   * @returns {string} variable as an icon name
   */
  const handleDetailsIcon = (props: any) => {
    let componentType;
    componentType = getComponentType(props);

    switch (componentType) {
      case ComponentsType.Temperature:
        // if (props.conditionComponentDirection === "change.window") {
        if (props.message.includes("inside")) {
          return tempMinIcon;
        } else {
          return tempMaxIcon;
        }

      case ComponentsType.Pressure:
        return pressureMaxIcon;

      case ComponentsType.AirQuality:
        return airMaxIcon;

      case ComponentsType.Humidity:
        return humiMaxIcon;

      default:
        break;
    }
  };

  /**
   * @function onSearchValueChange
   * Keep SearchBar value and update searchValue state
   *
   * @param {any} e
   */
  const onSearchValueChange = (e: any) => {
    setSearchValue(e.target.value);
  };

  /**
   * @function onCrossIconClick
   * Empty searchValue when cross icon is clicked
   */
  const onCrossIconClick = () => {
    setSearchValue("");
  };

  /**
   * @function filterData
   * Combined filtering functionality
   * Searching device Id from first character of a string
   * Handling symbolic chars while searching like *)(\?/[] etc.
   * Filtering with date time range
   * Update the filteredData state that is used in alarm table data
   */
  const filterData = () => {
    let fullData = data;

    // Date time range filtering
    if (dateTimeRange?.length) {
      if (dateTimeRange[0] === "" || dateTimeRange[1] === "") {
      } else {
        fullData = fullData.filter((item) => {
          const date = moment(item.datetimeInserted).format(dateTimeFormat);

          return moment(date).isBetween(
            dateTimeRange[0],
            dateTimeRange[1],
            null,
            "[]"
          );
        });
      }
    }

    // filtering upon search value
    if (searchValue) {
      const regexSymbolFormat = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]+/;

      if (searchValue === "") {
        setFilteredData(fullData);
      } else {
        if (fullData.length > 0) {
          if (regexSymbolFormat.test(searchValue)) {
            setFilteredData([]);
            return;
          } else {
            fullData = fullData.filter((item) =>
              item.deviceId.toLowerCase().match("^" + searchValue)
            );

            setFilteredData(fullData);
          }
        }
      }
    }

    setFilteredData(fullData);
  };

  /**
   * For combined filtering, filterData() is called depending on any of two states update
   * such as searchValue, dateTimeRange
   */
  useEffect(() => {
    filterData();
  }, [searchValue, dateTimeRange]);

  useEffect(() => {
    // When Admin component is rendered first, loadAlerts() is called
    loadAlerts();
  }, []);

  /**
   * @function loadAlerts
   * Load alerts functionality
   * Empty table data state as filteredData before API calling
   * Hits alert APIn
   * When data is loaded, updates necessary states within loadSpinner state
   */
  const loadAlerts = () => {
    setFilteredData([]);
    setLoaderSpinner(true);

    api<AdminTableResponse[]>("get", "alert", "", "")
      .then((data) => {
        let alertData: AdminTableResponse[] = data.data;
        setData(alertData);
        setSearchValue("");
        setDateTimeRange([]);

        setTimeout(() => {
          setFilteredData(alertData);
          setLoaderSpinner(false);
        }, 1000);
      })
      .catch((error) => {
        setLoaderSpinner(false);
      });
  };

  /**
   * @function onChange
   * Update dateTimeRange according to the onChange value of RangePicker
   * dateTimeString has two string value such as start and end date time
   *
   * @param {any} value - object from moment library
   * @param {string[]} dateTimeString - two string of date and time info together
   */
  const onChange = (value: any, dateTimeString: string[]) => {
    if (value === null) {
      setDateTimeRange([]);
    } else {
      setDateTimeRange(dateTimeString);
    }
  };

  return (
    <>
      <div className="right">
        {/* Inner header area starts */}
        <div className="top">
          <h1>Admin</h1>
          <button
            className="btn btn-red fw-normal btn-refresh"
            onClick={loadAlerts}
          >
            <img src={refreshWhiteIcon} alt="icon" /> Refresh List
          </button>
        </div>
        {/* Inner header area ends */}

        {/* Main content area starts */}
        <div className="main-content">
          <h4>
            <img src={carbonNotificationIcon} alt="icon" /> Alarms
          </h4>

          {/* Searching and filtering area starts */}
          <div className="top">
            <div className="container-fluid ps-2">
              {filteredData && (
                <div className="row">
                  <div className="col-md-12 col-lg-6 col-xl-6 col-xxl-6 pe-1 ps-1 pb-2">
                    <SearchBar
                      onChange={onSearchValueChange}
                      value={searchValue}
                      onCrossIconClick={onCrossIconClick}
                    />
                  </div>

                  <div className="col-md-12 col-lg-6 col-xl-6 col-xxl-6">
                    <div className="row">
                      <div className="col-md-12 ps-1 pe-1">
                        <div className="dropdown date-time-range">
                          <RangePicker
                            onChange={(value, dateString) =>
                              onChange(value, dateString)
                            }
                            value={
                              dateTimeRange.length
                                ? [
                                    moment(dateTimeRange[0]),
                                    moment(dateTimeRange[1]),
                                  ]
                                : [null, null]
                            }
                            format={dateTimeFormat}
                            showTime
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
          {/* Searching and filtering area starts */}

          {/* Loader spinner until data is ready */}
          {loaderSpinner && (
            <div className="centered">
              <Loader type="Circles" color="#DA291C" height={100} width={100} />
            </div>
          )}

          {/* Alarm table data */}
          {filteredData?.length > 0 && !loaderSpinner ? (
            <Table
              columns={columns}
              data={filteredData}
              showPagination={true}
              columnsFilter={true}
            />
          ) : !loaderSpinner ? (
            <h2 className="text-center">No alarm data available</h2>
          ) : (
            ""
          )}
        </div>
        {/* Main content area ends */}
      </div>
    </>
  );
};

export default Admin;
