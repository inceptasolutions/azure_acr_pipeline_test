/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { useEffect, useState } from "react";
import updateImage from "../../assets/images/update.png";
import api from "../../services/http";
import Table from "../../components/DeviceTable";
import SearchBar from "../../components/SearchBar";
import { DropdownButton, Dropdown } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { ColumnFilter } from "../../components/DeviceTable/ColumnFilter";
import Loader from "react-loader-spinner";
import { DeviceTableResponse } from "../../helpers/types";

const Home = () => {
  document.title = "Home Page | Smart Connect";

  const history = useHistory();

  const [data, setData] = useState<DeviceTableResponse[]>([]);
  const [searchValue, setSearchValue] = useState("");
  const [filterType, setFilterType] = useState("");
  const [filterStatus, setFilterStatus] = useState<string | null>("");
  const [filterLocation, setFilterLocation] = useState("");
  const [filteredData, setFilteredData] = useState<DeviceTableResponse[]>([]);
  const [status, setStatus] = useState("");
  const [allTypes, setAllTypes] = useState<string[]>([]);
  const [allLocation, setAllLocation] = useState<string[]>([]);
  const [loadSpinner, setLoadSpinner] = useState<boolean>(false);

  /**
   * Columns for device table that represent as table heading
   */
  const columns = [
    {
      Header: "Device ID",
      accessor: "deviceId",
      Filter: ColumnFilter,
      width: 50,
    },
    {
      Header: "Type",
      accessor: "type",
      Cell: (props: any) => <span>{formatHostname(props.value)}</span>,
      Filter: ColumnFilter,
      width: 50,
    },
    {
      Header: "Location",
      accessor: "location",
      Filter: ColumnFilter,
      width: 100,
    },
    {
      Header: "Status",
      accessor: "status",
      Cell: (props: any) => (
        <span className={`status ${props.value === "on" ? "onn" : "off"}`}>
          {props.value === "on" ? "On" : "Off"}
        </span>
      ),
      Filter: ColumnFilter,
      width: 80,
    },
    {
      accessor: "view_device",
      sortable: false,
      Cell: (props: any) => {
        return (
          <button
            className="btn btn-red-bordered shadow-none pe-1"
            onClick={() => handleDevice(props.row.cells[0].value)}
          >
            {" "}
            View Device{" "}
          </button>
        );
      },
      filterable: false,
      width: 100,
    },
  ];

  /**
   * @function handleDevice
   * When any device is selected to view the details,
   * go to the route /device-details/{id} using useHistory hook
   *
   * @param {string} id - device id
   */
  const handleDevice = (id: string) => {
    history.push({
      pathname: `/device-details/${id}`,
    });
  };

  /**
   * @function filterData
   * Combined filtering functionality
   * Filter by Types, locations, status as well as searching
   * Searching device Id from first character of a string
   * Handling symbolic chars while searching like *)(\? etc.
   * Update the filteredData state that is used in device table data
   */
  const filterData = () => {
    let fullData = data;

    if (filterType) {
      if (filterType !== "All types") {
        const actualType = reformatHostname(filterType);
        fullData = fullData.filter((item) => item.type === actualType);
      }
    }

    if (filterLocation) {
      if (filterLocation !== "All locations")
        fullData = fullData.filter((item) => item.location === filterLocation);
    }

    if (
      (filterStatus !== "All status" && filterStatus === "on") ||
      (filterStatus !== "All status" && filterStatus === "off") ||
      filterStatus === null
    ) {
      fullData = fullData.filter((item) => item.status === filterStatus);
    }

    if (searchValue) {
      const regexSymbolFormat = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]+/;

      if (searchValue === "") {
        setFilteredData(fullData);
      } else {
        if (fullData.length > 0) {
          if (regexSymbolFormat.test(searchValue)) {
            setFilteredData([]);
            return;
          } else {
            fullData = fullData.filter((item) =>
              item.deviceId.toLowerCase().match("^" + searchValue)
            );
          }

          setFilteredData(fullData);
        }
      }
    }

    setFilteredData(fullData);
  };

  /**
   * @function onSearchValueChange
   * Keep SearchBar value and update searchValue state
   *
   * @param {any} e
   */
  const onSearchValueChange = (e: any) => {
    setSearchValue(e.target.value);
  };

  /**
   * @function onCrossIconClick
   *  Empty searchValue when cross icon is clicked
   */
  const onCrossIconClick = () => {
    setSearchValue("");
  };

  useEffect(() => {
    // When Home component is rendered first, updateList() is called
    updateList();
  }, []);

  /**
   * For combined filtering, filterData() is called depending on any of four states update
   * such as filterType, filterLocation, filterStatus, searchValue
   */
  useEffect(() => {
    filterData();
  }, [filterType, filterLocation, filterStatus, searchValue]);

  /**
   * @function updateList
   * Update list functionality
   * Empty table data state as filteredData before API calling
   * Hits pageRefresh API
   * When data is loaded, updates necessary states within loadSpinner state
   */
  const updateList = () => {
    setFilteredData([]);
    setLoadSpinner(true);

    api<DeviceTableResponse[]>("get", "device/pageRefresh", "", "")
      .then((data) => {
        let deviceData = data.data;
        setData(deviceData);
        setFilteredData(deviceData);
        setFilterType("");
        setFilterLocation("");
        setStatus("");
        setSearchValue("");

        // Get all types list
        let typeArray = ["All types"];
        const types = new Set(
          deviceData.map((item) => {
            return formatHostname(item.type);
          })
        );
        typeArray = [...typeArray, ...Array.from(types)];
        setAllTypes(typeArray);

        // Get all locations list
        let locationArray = ["All locations"];
        const locations = new Set(
          deviceData.map((item) => {
            return item.location;
          })
        );
        locationArray = [...locationArray, ...Array.from(locations)];
        setAllLocation(locationArray);

        setLoadSpinner(false);
      })
      .catch((error) => {
        setLoadSpinner(false);
      });
  };

  /**
   * @function handleType
   * Update filterType state that is used for title in Type dropdown button
   *
   * @param {string} type
   */
  const handleType = (type: string) => {
    setFilterType(type);
  };

  /**
   * @function handleLocation
   * Update filterLocation state that is used for title in Location dropdown button
   *
   * @param {string} e - location name as string
   */
  const handleLocation = (e: string) => {
    const locationValue = e;
    setFilterLocation(locationValue);
  };

  /**
   * @function handleStatus
   * Update status state that is used for title in Status dropdown button
   *
   * @param {string} st - status on, off or all as a string
   */
  const handleStatus = (st: string) => {
    setStatus(st);

    let statusValue: string;
    if (st === "On") {
      statusValue = "on";
      setFilterStatus(statusValue);
    } else if (st === "Off") {
      statusValue = "off";
      setFilterStatus(statusValue);
    } else if (st === "All status") {
      setFilterStatus("All status");
    }
  };

  /**
   * @function formatHostname
   * Format type that removes 'host.' from every device type string
   *
   * @param {string} type - device type
   * @returns {string} type that represents on Type column cell of the device table
   */
  const formatHostname = (type: string) => {
    const str = type.split(".")[1];
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  /**
   * @function reformatHostname
   * Reformat type that adds 'host.' on every device type string
   *
   * @param {string} type - device type name
   * @returns {string} The actual device type name of the response
   */
  const reformatHostname = (type: string) => {
    const str = type.toLowerCase();
    return `host.${str}`;
  };

  return (
    <>
      <div className="right">
        {/* Inner header section starts */}
        <div className="top">
          <h1>Devices</h1>
          <button
            className="btn btn-lightgray-bordered mb-3"
            onClick={updateList}
          >
            <img src={updateImage} alt="icon" /> Update List
          </button>
        </div>
        {/* Inner header section ends */}

        {/* Content Area starts */}
        <div className="main-content">
          {/* Searching and filtering area starts */}
          <div className="top">
            <div className="container-fluid ps-2">
              {filteredData && (
                <div className="row">
                  <div className="col-md-12 col-lg-12 col-xl-4 pe-0 pb-2">
                    <SearchBar
                      onChange={onSearchValueChange}
                      value={searchValue}
                      onCrossIconClick={onCrossIconClick}
                    />
                  </div>
                  <div className="col-md-12 col-lg-12 col-xl-8 dropdown-buttons">
                    <div className="row">
                      <div className="col-md-4 pe-0 pb-2">
                        <div className="dropdown">
                          <DropdownButton
                            variant="light"
                            title={
                              filterType === "" ? "Filter by type" : filterType
                            }
                          >
                            {allTypes.map((type, idx) => (
                              <Dropdown.Item
                                key={idx}
                                onClick={() => handleType(type)}
                              >
                                {" "}
                                {type}{" "}
                              </Dropdown.Item>
                            ))}
                          </DropdownButton>
                        </div>
                      </div>

                      <div className="col-md-4 pe-0 pb-2">
                        <div className="dropdown">
                          <DropdownButton
                            variant="light"
                            title={
                              filterLocation === ""
                                ? "Filter by location"
                                : filterLocation
                            }
                          >
                            {allLocation.map((location, idx) => (
                              <Dropdown.Item
                                key={idx}
                                onClick={() => handleLocation(location)}
                              >
                                {" "}
                                {location}{" "}
                              </Dropdown.Item>
                            ))}
                          </DropdownButton>
                        </div>
                      </div>

                      <div className="col-md-4 pe-0 pb-1">
                        <div className="dropdown">
                          <DropdownButton
                            variant="light"
                            title={status === "" ? "Filter by status" : status}
                          >
                            {["All status", "On", "Off"].map((st, idx) => (
                              <Dropdown.Item
                                key={idx}
                                onClick={() => handleStatus(st)}
                              >
                                {" "}
                                {st}{" "}
                              </Dropdown.Item>
                            ))}
                          </DropdownButton>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
          {/* Searching and filtering area ends */}

          {/* Load spinner until data is ready */}
          {loadSpinner && (
            <div className="centered">
              <Loader type="Circles" color="#DA291C" height={100} width={100} />
            </div>
          )}

          {/* Table data */}
          {filteredData.length > 0 && (
            <Table
              columns={columns}
              data={filteredData}
              showPagination={true}
              columnsFilter={true}
            />
          )}
        </div>
        {/* Content Area ends */}
      </div>
    </>
  );
};

export default Home;
