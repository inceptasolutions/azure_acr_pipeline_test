import AppRoutes from "./routes";
import { Header } from "../src/components/Shared/Header";
import Sidebar from "../src/components/Shared/Sidebar";
import { BrowserRouter as Router } from "react-router-dom";
import { AuthenticatedTemplate, UnauthenticatedTemplate } from "@azure/msal-react";
import  { SignInButton } from './components/signInButton';
import logo from "./assets/images/logo.png";

function App() {
 
  return (
    <div className="App">
        <AuthenticatedTemplate>
        <Router>
          {/* Common Header section for all pages */}
          <Header />
          <div className="main-wrapper">
            {/* Common Sidebar section for all pages */}
            <Sidebar />
            {/* All the routes are defined here like /home, /device-details, /admin etc.*/}
            <AppRoutes />
          </div>
        </Router>
        </AuthenticatedTemplate>
        <UnauthenticatedTemplate>
              <div className="signIn" >
                <div/>
                <img className="logo" src={logo} alt="logo" />
                <SignInButton />
              </div>
        </UnauthenticatedTemplate>
    </div>
  );
}

export default App;
