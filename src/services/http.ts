import axios, { Method, AxiosResponse } from "axios";
import { loginRequest, msalConfig } from '../authConfig';
import { InteractionRequiredAuthError, PopupRequest, PublicClientApplication, SilentRequest } from "@azure/msal-browser";
import { useIsAuthenticated } from "@azure/msal-react";

const myMSALObj = new PublicClientApplication(msalConfig);

// Default config options
const defaultOptions = {
  baseURL: "https://smartconnect-iot-exp-api-autodiscovery.ca-c1.cloudhub.io/riss-iot-exp-api",
  headers: {
    'Access-Control-Allow-Origin' : '*',
  }
};

// Create instance
let axiosInstance = axios.create(defaultOptions);

const getToken = () => {
  const Accounts = myMSALObj.getAllAccounts();
  if(Accounts.length != 0){
    myMSALObj.setActiveAccount(Accounts[0])
    const Request = {
      ...loginRequest
    } 
  myMSALObj.acquireTokenSilent(Request)
    .then(data => { 
      localStorage.setItem('token',data.accessToken);
    })
    .catch( error => {
      console.log(error)
    });
 }
}

getToken();

// Set the AUTH token for any request
axiosInstance.interceptors.request.use(function (config) {
  const token = localStorage.getItem('token');
  config.headers.Authorization =  token ? `Bearer ${token}` : '';
  return config;
});

const request = <T>(
  method: Method,
  url: string,
  params?: any,
  data?: any
): Promise<AxiosResponse<T>> => {
return axiosInstance.request<T>({
    method,
    url,
    params,
    data,
  });
};

export default request;