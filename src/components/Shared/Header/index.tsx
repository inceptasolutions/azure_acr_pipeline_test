import logo from "../../../assets/images/logo.png";
import avatar from "../../../assets/images/carbon_user-avatar.png";
import { Dropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import signOut from "../../../assets/images/signOut.png"

import { useMsal } from "@azure/msal-react";



function handleLogout(instance: { logoutRedirect: () => Promise<any>; }) {
  instance.logoutRedirect().catch(e => {
      console.error(e);
  });
}


export const Header = () => {

  const { instance, accounts } = useMsal();
  
  const name = accounts[0] && accounts[0].name;
  
  
  return (
    <>
      {/* Top header section starts */}
      <div className="header">
        <div className="logo">
          <Link to="/">
            <img src={logo} className="image" alt="logo" />
          </Link>
        </div>
        <div className="user">
          <div className="dropdown username">
            <Dropdown>
              <Dropdown.Toggle id="dropdown-basic">
                { name ? name : "user" } <img src={avatar} alt="icon" />
              </Dropdown.Toggle>
            </Dropdown>
          </div>
          <img src={signOut} className="signout" onClick={() => handleLogout(instance)}/>
        </div>
        
      </div>
      {/* Top header section ends */}
    </>
  );
};
