import { useEffect, useState } from "react";
import devices from "../../../assets/images/device.png";
import admin from "../../../assets/images/admin.png";
import navMenu from "../../../assets/images/navMenu.png";
import { Link } from "react-router-dom";

const Sidebar = () => {
  const [sidebarCollapsed, setSidebarCollapsed] = useState(true);

  /**
   * @function handleSidebarCollapse
   * Update sidebarCollapsed state while click event occurs
   */
  const handleSidebarCollapse = () => {
    setSidebarCollapsed(!sidebarCollapsed);
  };

  /**
   * @function handleResize
   * Window resize functionality upon sidebarCollapse state changing
   */
  const handleResize = () => {
    const width = window.innerWidth;
    if (width < 992) {
      setSidebarCollapsed(false);
    } else {
      setSidebarCollapsed(true);
    }
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  });

  useEffect(() => {
    if (window.innerWidth < 992) {
      setSidebarCollapsed(false);
    }
  }, []);

  return (
    <>
      {/* Sidebar menu section starts */}
      <div
        className={`left ${
          sidebarCollapsed ? "normal-width" : "collapsed-width"
        } s`}
      >
        <span className="px-1 my-2" onClick={handleSidebarCollapse}>
          <img width="30" src={navMenu} className="text-white" alt="icon" />
        </span>
        <ul className="mt-4">
          <li>
            <Link className={`${sidebarCollapsed ? "" : "collapsed"}`} to="/">
              <span></span>
              <img src={devices} alt="icon" />
              Devices
            </Link>
          </li>
          <li>
            <Link
              className={`${sidebarCollapsed ? "" : "collapsed"}`}
              to="/admin"
            >
              <span></span>
              <img src={admin} alt="icon" />
              Admin
            </Link>
          </li>
        </ul>
      </div>
      {/* Sidebar menu section ends */}
    </>
  );
};

export default Sidebar;
