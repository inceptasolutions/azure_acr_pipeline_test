/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, useState } from "react";
import removeIcon from "../../assets/images/remove.png";
import scheduleIcon from "../../assets/images/carbon_event-schedule.png";
import { TimePicker } from "antd";
import moment from "moment";
import api from "../../services/http";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import { Day, DayMap, DaysOfTheWeek, ScheduleRequest } from "../../helpers/types";

const format = "HH:mm";

/**
 * @type {object}
 * 
 * ScheduleDetailsProps is for React component props that come from React parent component
 */
type ScheduleDetailsProps = {
  schedule: any[];
  goToRule: Function;
  rulesComponents: string[];
  loadDeviceData: Function;
};

toast.configure();

const ScheduleDetails = ({
  schedule,
  goToRule,
  rulesComponents,
  loadDeviceData,
}: ScheduleDetailsProps) => {
  const data: DayMap = {
    weekdays: [
      {
        title: DaysOfTheWeek.Monday,
        startTime: "",
        endTime: "",
        toggle: true,
        isChanged: false,
      },
      {
        title: DaysOfTheWeek.Tuesday,
        startTime: "",
        endTime: "",
        toggle: true,
        isChanged: false,
      },
      {
        title: DaysOfTheWeek.Wednesday,
        startTime: "",
        endTime: "",
        toggle: true,
        isChanged: false,
      },
      {
        title: DaysOfTheWeek.Thursday,
        startTime: "",
        endTime: "",
        toggle: true,
        isChanged: false,
      },
      {
        title: DaysOfTheWeek.Friday,
        startTime: "",
        endTime: "",
        toggle: true,
        isChanged: false,
      },
    ],
    weekends: [
      {
        title: DaysOfTheWeek.Saturday,
        startTime: "",
        endTime: "",
        toggle: true,
        isChanged: false,
      },
      {
        title: DaysOfTheWeek.Sunday,
        startTime: "",
        endTime: "",
        toggle: true,
        isChanged: false,
      },
    ],
  };

  const [scheduleData, setScheduleData] = useState<DayMap>(data);
  const [sameTime, setSameTime] = useState(false);
  const param = useParams<any>();
  const timeRef = useRef(null);
  const [loadSpinner, setLoadSpinner] = useState(false);

  /**
   * @function preFillData
   * When schedule state is updated, preFillData is called upon useEffect hook.
   * Update scheduleData state according to the schedule state.
   * When scheduleData is updated, the start and end time of available days are filled in time input field
   */
  const preFillData = () => {
    let temp = { ...data };

    for (let i = 0; i < schedule.length; i++) {
      const day = schedule[i].days[0];
      if (day === DaysOfTheWeek.Saturday || day === DaysOfTheWeek.Sunday) {
        const index = temp.weekends.findIndex((entry) => entry.title === day);
        temp.weekends[index].startTime = schedule[i].startTime;
        temp.weekends[index].endTime = schedule[i].endTime;
        temp.weekends[index].toggle =
          schedule[i].startTimeAction === "action.powerOff" ? false : true;
        temp.weekends[index].isChanged = true;
      } else {
        const index = temp.weekdays.findIndex((entry) => entry.title === day);
        temp.weekdays[index].startTime = schedule[i].startTime;
        temp.weekdays[index].endTime = schedule[i].endTime;
        temp.weekdays[index].toggle =
          schedule[i].startTimeAction === "action.powerOff" ? false : true;
        temp.weekdays[index].isChanged = true;
      }
    }

    setScheduleData(temp);
  };

  useEffect(() => {
    if (schedule) {
      // If schedule state is updated, call preFillData function
      preFillData();
    }
  }, [schedule]);

  useEffect(() => {
    if (sameTime) {
      // If sameTime state is updated, call updateOnSameData function
      updateOnSameData();
    }
  }, [sameTime]);

  /**
   * @function formatDayName
   * Apply uppercase to the first character of every day name
   * 
   * @param {string} name - day name
   * @returns {string} day name with uppercase on first character
   */
  const formatDayName = (name: string) => {
    return name.charAt(0).toUpperCase() + name.slice(1);
  };

  /**
   * @function handleSameTime
   * Update sameTime state while input checkbox is changed from the UI in the schedule section
   * 
   * @param {any} e
   */
  const handleSameTime = (e: any) => {
    setSameTime(e.target.checked);
  };

  /**
   * @function cancelSchedule
   * When Reset All button is clicked from the UI in the schedule section,
   * update scheduleData state with default empty data
   */
  const cancelSchedule = () => {
    setScheduleData(data);
  };

  /**
   * @function handleToggle
   * Handle on/off toggle while adding or updating time of a day's input.
   * if the same time is checked, assign checked value to all the days of same day type.
   * Otherwise, assign the checked value on the selected date
   * 
   * @param {any} e
   * @param {string} dayType - weekdays or weekends
   * @param {string} day - day name
   */
  const handleToggle = (e: any, dayType: string, day: string) => {
    let temp = { ...scheduleData };

    if (sameTime) {
      for (let i = 0; i < temp[dayType].length; i++) {
        temp[dayType][i].toggle = e.target.checked;
      }
    } else {
      const index = temp[dayType].findIndex((entry) => entry.title === day);
      temp[dayType][index].toggle = e.target.checked;
    }

    setScheduleData(temp);
  };

  /**
   * @function onChange
   * Find the actual day of input.
   * Check if the sameTime is true or not
   * Handle if the start time and end time is same or less or more than the end time
   * Update the tempData accordingly
   * After looping all the days, update scheduleData with tempData
   * 
   * @param {any} time 
   * @param {string} timeString - moment time string
   * @param {string} timeType - startTime or endTime
   * @param {string} day - day name
   * @param {string} dayType - weekdays or weekends
   * @returns when condition fails or update scheduleData state
   */
  const onChange = (
    time: any,
    timeString: string,
    timeType: string,
    day: string,
    dayType: string
  ) => {
    let tempData: DayMap = { ...scheduleData };
    const index = tempData[dayType].findIndex((entry) => entry.title === day);

    if (sameTime) {
      if (timeType === "startTime") {
        for (let i = 0; i < tempData[dayType].length; i++) {
          if (
            moment(timeString, format).isSameOrAfter(
              moment(tempData[dayType][i]?.endTime, format)
            )
          ) {
            toast.error(
              "Sorry, Start time can not be greater than the end time"
            );
            return;
          }
          tempData[dayType][i].startTime = timeString;
          tempData[dayType][i].isChanged = true;
        }
      } else {
        for (let i = 0; i < tempData[dayType].length; i++) {
          if (
            moment(timeString, format).isSameOrBefore(
              moment(tempData[dayType][i]?.startTime, format)
            )
          ) {
            toast.error(
              "Sorry, End time can not be lesser than the start time"
            );
            return;
          }
          tempData[dayType][i].endTime = timeString;
          tempData[dayType][i].isChanged = true;
        }
      }
    } else {
      if (timeType === "startTime") {
        if (
          moment(timeString, format).isSameOrAfter(
            moment(tempData[dayType][index]?.endTime, format)
          )
        ) {
          toast.error("Sorry, Start time can not be greater than the end time");
          return;
        }
        tempData[dayType][index].startTime = timeString;
        tempData[dayType][index].isChanged = true;
      } else {
        if (
          moment(timeString, format).isSameOrBefore(
            moment(tempData[dayType][index]?.startTime, format)
          )
        ) {
          toast.error("Sorry, End time can not be lesser than the start time");
          return;
        }
        tempData[dayType][index].endTime = timeString;
        tempData[dayType][index].isChanged = true;
      }
    }

    setScheduleData(tempData);
  };

  /**
   * @function insertHeaderOnDate
   * Make a fix position for the header title on every input field of days.
   * Both of Start Time and End Time are styled with CSS according to the UX design
   */
  const insertHeaderOnDate = () => {
    var pickerStart = document.getElementsByClassName("time-picker-start");
    var pickerEnd = document.getElementsByClassName("time-picker-end");

    for (let i = 0; i < pickerStart.length; i++) {
      pickerStart[i].insertAdjacentHTML(
        "afterend",
        `<span style="position: absolute; top: 0px; left: 13px;">Start Time</span>`
      );
    }

    for (let i = 0; i < pickerEnd.length; i++) {
      pickerEnd[i].insertAdjacentHTML(
        "afterend",
        `<span style="position: absolute; top: 0px; left: 13px;">End Time</span>`
      );
    }
  };

  useEffect(() => {
    insertHeaderOnDate();
  }, []);

  /**
   * @function formatDataForRequest
   * Make ready schedule data for sending to the API as request body structure.
   * If the parameter data is empty, returns
   * It performs for a single day at a time
   * 
   * @param {Day} tempSchedule - interface Day
   * @returns {ScheduleRequest} interface ScheduleRequest
   */
  const formatDataForRequest = (tempSchedule: Day) => {
    let scheduleDataReq: ScheduleRequest;
    if (tempSchedule.startTime !== "" && tempSchedule.endTime !== "") {
      scheduleDataReq = {
        deviceId: param?.id,
        scheduleName: "schedule " + tempSchedule.title,
        start: {
          action: {
            componentId: "0",
            type: tempSchedule.toggle ? "powerOn" : "powerOff",
            time: tempSchedule.startTime,
          },
        },
        end: {
          action: {
            componentId: "0",
            type: !tempSchedule.toggle ? "powerOn" : "powerOff",
            time: tempSchedule.endTime,
          },
        },
        during: {
          weekdays: [tempSchedule.title],
        },
      };
      return scheduleDataReq;
    }
    return;
  };

  /**
   * @function handleSchedule
   * Create empty variable named requestBody that takes schedule data from state.
   * Loop through for weekdays and weekends.
   * Call formatDataForRequest function to get the consistent data request for the API.
   * If data is okay, hit the schedule API with requestBody.
   * Update the necessary states and call the loadDeviceData() function to load the updated data
   * setTimeout is used for considering time in data processing in backend 
   */
  const handleSchedule = () => {
    let requestBody: ScheduleRequest[] = [];
    for (let i = 0; i < scheduleData.weekdays.length; i++) {
      if (scheduleData.weekdays[i].isChanged) {
        const tempSchedule = scheduleData.weekdays[i];
        const formattedData = formatDataForRequest(tempSchedule);
        if (formattedData) {
          requestBody.push(formattedData);
        }
      }
    }

    for (let i = 0; i < scheduleData.weekends.length; i++) {
      if (scheduleData.weekends[i].isChanged) {
        const tempSchedule = scheduleData.weekends[i];
        const formattedData = formatDataForRequest(tempSchedule);
        if (formattedData) {
          requestBody.push(formattedData);
        }
      }
    }

    if (requestBody.length) {
      setLoadSpinner(true);
      api<ScheduleRequest>("post", "schedule", "", requestBody).then(
        (data) => {
          setTimeout(() => {
            toast.success("Schedule saved successfully");

            setTimeout(() => {
              setSameTime(false);
              setLoadSpinner(false);
              insertHeaderOnDate();
              loadDeviceData();
            }, 3000);
          }, 5000);
        },
        (error) => {
          setLoadSpinner(false);
          insertHeaderOnDate();
          toast.error("Sorry, Schedule couldn't be saved");
        }
      );
    }
  };

  /**
   * @function bindTimepickerToSchedule
   * Find the actual day of input.
   * Assign start time or end time upon condition.
   * Handle if date is valid or invalid
   *
   * @param {string} timeType 
   * @param {string} day 
   * @param {string} dayType 
   * @returns {string | null} as date string or null when date is invalid
   */
  const bindTimepickerToSchedule = (
    timeType: string,
    day: string,
    dayType: string
  ) => {
    const temp = { ...scheduleData };
    let date;
    const index = temp[dayType].findIndex((entry) => entry.title === day);
    if (timeType === "startTime") {
      date = temp[dayType][index].startTime;
    } else {
      date = temp[dayType][index].endTime;
    }
    date = moment(date, format);
    if (date.isValid()) {
      return date;
    } else {
      return null;
    }
  };

  /**
   * @function updateOnSameData
   * When checkbox is active for same time on weekdays and weekends,
   * all the days fields are filled with the same time as typed
   */
  const updateOnSameData = () => {
    const temp = { ...scheduleData };

    for (let i = 1; i < temp.weekdays.length; i++) {
      temp.weekdays[i].startTime = temp.weekdays[0].startTime;
      temp.weekdays[i].endTime = temp.weekdays[0].endTime;
      temp.weekdays[i].toggle = temp.weekdays[0].toggle;
      temp.weekdays[i].isChanged = true;
    }
    for (let i = 1; i < temp.weekends.length; i++) {
      temp.weekends[i].startTime = temp.weekends[0].startTime;
      temp.weekends[i].endTime = temp.weekends[0].endTime;
      temp.weekends[i].toggle = temp.weekends[0].toggle;
      temp.weekends[i].isChanged = true;
    }

    setScheduleData(temp);
  };

  return (
    <>
      <div className="device-schedule">
        {/* Device Schedule title area starts*/}
        <div className="title-area">
          <h4>
            <img src={scheduleIcon} alt="icon" /> Device Schedule
          </h4>

          <div className="d-flex">
            {rulesComponents.length > 0 && (
              <button
                className="btn btn-red shadow-none me-3"
                onClick={() => goToRule()}
              >
                {" "}
                Go to Rule
              </button>
            )}

            <button
              className="btn btn-lightgray-bordered shadow-none me-3"
              onClick={cancelSchedule}
            >
              <img src={removeIcon} alt="icon" /> Reset All
            </button>
            <button
              className="btn btn-red shadow-none"
              onClick={handleSchedule}
              disabled={loadSpinner}
            >
              {" "}
              Save Schedule
            </button>
          </div>
        </div>
        {/* Device Schedule title area ends*/}

        {loadSpinner && (
          <div className="loader-sm">
            <Loader type="Circles" color="#DA291C" height={100} width={100} />
          </div>
        )}

        {!loadSpinner && (
          <>
            <p>
              Ensure this device only operates during predetermined times of the
              week. If no schedule is set, device will always be ON.
            </p>
            <label className="custom-checkbox">
              Same time for weekdays and weekends <br />
              <small>
                Set the same time for weekdays and a different time for weekends
              </small>
              <input type="checkbox" onChange={handleSameTime} />
              <span className="checkmark"></span>
            </label>
          </>
        )}

        {!loadSpinner && (
          <div className="weekends-weekdays">
            <div className="container-fluid">
              {/* Weekdays group starts here */}
              <div className="row weekdays">
                <div className="col-12">Weekdays</div>
              </div>

              {/* Loop through for weekdays upon scheduleData */}
              {scheduleData.weekdays.map((entry, idx) => {
                return (
                  <div className="row" key={idx}>
                    <div className="col-lg-2 mb-3 mb-lg-0 align-items-center d-flex">
                      <label>{formatDayName(entry.title)}</label>
                    </div>

                    {/* Weekdays start time starts */}
                    <div className="col-lg-4 mb-3 mb-lg-0">
                      <div className="start-time dropdown-buttons">
                        <div className="dropdown">
                          <TimePicker
                            disabled={idx !== 0 && sameTime}
                            ref={timeRef}
                            value={bindTimepickerToSchedule(
                              "startTime",
                              entry.title,
                              "weekdays"
                            )}
                            className="btn btn-secondary dropdown button time-picker time-picker-start"
                            format={format}
                            onChange={(time, timeString) =>
                              onChange(
                                time,
                                timeString,
                                "startTime",
                                entry.title,
                                "weekdays"
                              )
                            }
                          />
                        </div>
                      </div>
                    </div>
                    {/* Weekdays start time ends */}

                    {/* Weekdays end time starts */}
                    <div className="col-lg-4 mb-3 mb-lg-0">
                      <div className="end-time dropdown-buttons">
                        <div className="dropdown">
                          <TimePicker
                            disabled={idx !== 0 && sameTime}
                            value={bindTimepickerToSchedule(
                              "endTime",
                              entry.title,
                              "weekdays"
                            )}
                            className="btn btn-secondary dropdown button time-picker time-picker-end"
                            format={format}
                            onChange={(time, timeString) =>
                              onChange(
                                time,
                                timeString,
                                "endTime",
                                entry.title,
                                "weekdays"
                              )
                            }
                          />
                        </div>
                      </div>
                    </div>
                    {/* Weekdays end time ends */}

                    {/* weekdays toggle on/off starts here */}
                    <div className="col-lg-2 mb-3 mb-lg-0 d-flex justify-content-end align-items-center">
                      <div className="form-check form-switch">
                        <label
                          className="form-check-label me-5"
                          style={{ fontSize: "18px", fontWeight: "normal" }}
                        >
                          OFF
                        </label>
                        <input
                          disabled={idx !== 0 && sameTime}
                          className="form-check-input float-none"
                          type="checkbox"
                          id="flexSwitchCheckDefault"
                          checked={entry.toggle}
                          onChange={(e) =>
                            handleToggle(e, "weekdays", entry.title)
                          }
                        />
                        <label
                          className="form-check-label ms-2"
                          style={{ fontSize: "18px", fontWeight: "normal" }}
                        >
                          ON
                        </label>
                      </div>
                    </div>
                    {/* Weekdays toggle on/off ends here */}
                  </div>
                );
              })}

              {/* Weekdays group ends here */}

              {/* Weekends group starts here */}
              <div className="row weekdays">
                <div className="col-12">Weekends</div>
              </div>

              {/* Loop through for weekends upon scheduleData */}
              {scheduleData.weekends.map((entry, idx) => {
                return (
                  <div className="row" key={idx}>
                    <div className="col-lg-2 mb-3 mb-lg-0 align-items-center d-flex">
                      <label>{formatDayName(entry.title)}</label>
                    </div>

                    {/* Weekends start time starts */}
                    <div className="col-lg-4 mb-3 mb-lg-0">
                      <div className="start-time dropdown-buttons">
                        <div className="dropdown">
                          <TimePicker
                            disabled={idx !== 0 && sameTime}
                            value={bindTimepickerToSchedule(
                              "startTime",
                              entry.title,
                              "weekends"
                            )}
                            className="btn btn-secondary dropdown button time-picker time-picker-start"
                            id="time-picker"
                            format={format}
                            onChange={(time, timeString) =>
                              onChange(
                                time,
                                timeString,
                                "startTime",
                                entry.title,
                                "weekends"
                              )
                            }
                          />
                        </div>
                      </div>
                    </div>
                    {/* Weekends start time ends */}

                    {/* Weekends end time starts */}
                    <div className="col-lg-4 mb-3 mb-lg-0">
                      <div className="end-time dropdown-buttons">
                        <div className="dropdown">
                          <TimePicker
                            disabled={idx !== 0 && sameTime}
                            value={bindTimepickerToSchedule(
                              "endTime",
                              entry.title,
                              "weekends"
                            )}
                            className="btn btn-secondary dropdown button time-picker time-picker-end"
                            id="time-picker"
                            format={format}
                            onChange={(time, timeString) =>
                              onChange(
                                time,
                                timeString,
                                "endTime",
                                entry.title,
                                "weekends"
                              )
                            }
                          />
                        </div>
                      </div>
                    </div>
                    {/* Weekends end time ends */}

                    {/* Weekends toggle on/off starts here */}
                    <div className="col-lg-2 mb-3 mb-lg-0 d-flex justify-content-end align-items-center">
                      <div className="form-check form-switch">
                        <label
                          className="form-check-label me-5"
                          style={{ fontSize: "18px", fontWeight: "normal" }}
                        >
                          OFF
                        </label>
                        <input
                          disabled={idx !== 0 && sameTime}
                          className="form-check-input float-none"
                          type="checkbox"
                          id="flexSwitchCheckDefault"
                          checked={entry.toggle}
                          onChange={(e) =>
                            handleToggle(e, "weekends", entry.title)
                          }
                        />
                        <label
                          className="form-check-label ms-2"
                          style={{ fontSize: "18px", fontWeight: "normal" }}
                        >
                          ON
                        </label>
                      </div>
                    </div>
                    {/* Weekends toggle on/off ends here */}
                  </div>
                );
              })}
              {/* Weekends group ends here */}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default ScheduleDetails;
