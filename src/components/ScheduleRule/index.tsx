import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import addDeviceIcon from "../../assets/images/add-device.png";
import RuleDetails from "../RuleDetails/RuleDetails";
import ScheduleDetails from "../ScheduleDetails/ScheduleDetails";
import api from "../../services/http";
import { DeviceTableResponse } from "../../helpers/types";

export default function ScheduleRule({ deviceId, rulesComponents }: any) {
  const [isSchedule, setIsSchedule] = useState(false);
  const [isRule, setIsRule] = useState(false);
  const [scheduleData, setScheduleData] = useState<any[]>([]);
  const [ruleData, setRuleData] = useState<any[]>([]);

  const location = useLocation<any>();

  useEffect(() => {
    // When location is changed, loadDeviceData() is called to align the updated device data
    loadDeviceData();
  }, [location]);

  /**
   * @function goToSchedule
   * This function goes to next child component
   * and change state depending on 'Go to schedule' button click event
   */
  const goToSchedule = () => {
    setIsSchedule(true);
    setIsRule(false);
  };

  /**
   * @function goToRule
   * This function goes to next child component
   * and change state depending on 'Go to rule' button click event
   */
  const goToRule = () => {
    setIsRule(true);
    setIsSchedule(false);
  };

  /**
   * @function loadDeviceData
   * Load device data functionality.
   * Hits pageRefresh API.
   * When data is loaded, update necessary states.
   * Find the selected device matching the deviceId.
   * Check if device has rule or schedule and update state
   */
  const loadDeviceData = () => {
    api<DeviceTableResponse[]>("get", "device/pageRefresh", "", "").then(
      (data) => {
        const allData = data.data;

        const device: any = allData.find(
          (entry) => entry.deviceId === deviceId
        );
        const deviceRules: any[] = device.rule;
        const deviceSchedule: any[] = device.schedule;

        if (deviceRules.length) {
          setIsRule(true);
          setIsSchedule(false);
          setRuleData(deviceRules);
          setScheduleData([]);
        } else if (deviceSchedule.length) {
          setIsSchedule(true);
          setIsRule(false);
          setScheduleData(deviceSchedule);
          setRuleData([]);
        } else {
          setScheduleData([]);
          setRuleData([]);
        }
      }
    );
  };

  return (
    <>
      {/* If any device has no schedule or no rules, this section renders*/}
      {!isSchedule && !isRule && (
        <div className="device-schedule">
          <div className="title-area">
            <h4>
              <img src={addDeviceIcon} alt="icon" /> Add Device Schedule or
              Rules
            </h4>
            <div className="d-flex">
              <button
                className="btn btn-red shadow-none mr-3"
                onClick={() => setIsSchedule(true)}
              >
                {" "}
                Set Schedule
              </button>
              {rulesComponents.length > 0 && (
                <button
                  className="btn btn-red shadow-none"
                  onClick={() => setIsRule(true)}
                >
                  {" "}
                  Set Device Rules
                </button>
              )}
            </div>
          </div>
          <p>
            Set a schedule or conditional rules for when the device should
            operate.
          </p>
        </div>
      )}

      {/* If a device has schedule and no rule, this component renders */}
      {isSchedule && !isRule && (
        <ScheduleDetails
          schedule={scheduleData}
          goToRule={goToRule}
          rulesComponents={rulesComponents}
          loadDeviceData={loadDeviceData}
        />
      )}

      {/* If a device has rule and no schedule, this component renders */}
      {isRule && !isSchedule && (
        <RuleDetails
          rule={ruleData}
          goToSchedule={goToSchedule}
          deviceId={deviceId}
          rulesComponents={rulesComponents}
          loadDeviceData={loadDeviceData}
        />
      )}
    </>
  );
}
