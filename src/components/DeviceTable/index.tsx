import React, { useState } from "react";
import { useSortBy, useTable, usePagination, useFilters } from "react-table";
import sortIcon from "../../assets/images/sort-icon.png";
import leftAngleIcon from "../../assets/images/left-angle.png";
import rightAngleIcon from "../../assets/images/right-angle.png";
import { ColumnFilter } from "./ColumnFilter";

export default function Table({
  columns,
  data,
  showPagination,
  columnsFilter,
}: any) {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const defaultColumn = React.useMemo(
    () => ({
      Filter: ColumnFilter,
    }),
    []
  );

  // Use the state and functions returned from useTable to build your UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    nextPage,
    previousPage,
    canPreviousPage,
    canNextPage,
    state,
    pageOptions,
    gotoPage,
    pageCount,
  }: any = useTable(
    {
      columns,
      data,
      defaultColumn,
    },
    useFilters,
    useSortBy,
    usePagination
  );

  const { pageIndex, pageSize } = state;

  return (
    <>
      <table {...getTableProps()} className="table custom-table">
        {/* Table header/column area */}
        <thead>
          {headerGroups.map((headerGroup: any) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column: any) => (
                <th
                  width={column.width}
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                >
                  {column.render("Header")}
                  {column.sortable === false ? (
                    ""
                  ) : (
                    <>
                      <button className="border-0 bg-transparent">
                        {" "}
                        <img
                          style={{ marginLeft: "5px" }}
                          src={sortIcon}
                          alt="icon"
                        />
                      </button>
                    </>
                  )}
                  {column.filterable === false
                    ? ""
                    : columnsFilter && (
                        <div>
                          {isOpen && column.canFilter
                            ? column.render("Filter")
                            : null}
                        </div>
                      )}
                </th>
              ))}
            </tr>
          ))}
        </thead>

        {/* Table body area */}
        <tbody {...getTableBodyProps()}>
          {page.map((row: any) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell: any) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>

      {/* Pagination section starts */}
      {showPagination && (
        <div className="bottom">
          <p>
            Showing{" "}
            {data.length === 0
              ? 0
              : pageIndex === 0
              ? 1
              : pageIndex * pageSize + 1}{" "}
            to{" "}
            {data.length === 0
              ? 0
              : pageIndex === 0 && pageSize > data.length
              ? data.length
              : pageSize
              ? pageIndex * pageSize + pageSize > data.length
                ? data.length
                : pageIndex * pageSize + pageSize
              : ""}{" "}
            of {data.length} devices
          </p>

          {/* Pagination icon/number part starts */}
          <nav aria-label="Page navigation example">
            <ul className="pagination">
              <li className="page-item">
                <button
                  className="page-link"
                  aria-label="Previous"
                  onClick={() => previousPage()}
                  disabled={!canPreviousPage}
                >
                  <img src={leftAngleIcon} alt="icon" />
                </button>
              </li>
              {pageOptions.map((num: number) => {
                return (
                  <li key={num} className="page-item">
                    <button
                      className={`page-link ${
                        num === pageIndex ? "active" : ""
                      }`}
                      onClick={() => gotoPage(num)}
                      disabled={
                        num === 0
                          ? !canPreviousPage
                          : num === pageCount - 1
                          ? !canNextPage
                          : false
                      }
                    >
                      {num + 1}
                    </button>
                  </li>
                );
              })}

              <li className="page-item">
                <button
                  className="page-link"
                  aria-label="Next"
                  onClick={() => nextPage()}
                  disabled={!canNextPage}
                >
                  <img src={rightAngleIcon} alt="icon" />
                </button>
              </li>
            </ul>
          </nav>
          {/* Pagination icon/number part ends */}
        </div>
      )}
      {/* Pagination section starts */}
    </>
  );
}
