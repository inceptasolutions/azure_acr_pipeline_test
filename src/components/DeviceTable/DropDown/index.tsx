import { useState } from "react";

export default function Dropdown({
  title,
  options,
  selectedValue,
  onItemClick,
}: any) {
  const [isOpen, setOpen] = useState(false);

  return (
    <div className="col-lg-9 col-xl-6 dropdown-buttons">
      <div className="row">
        <div className="col-md-4 ps-1 pe-1">
          <div className="dropdown">
            <button
              className="btn btn-secondary dropdown-toggle"
              type="button"
              id="dropdownMenuButton1"
              data-bs-toggle="dropdown"
              aria-expanded="false"
              onClick={() => setOpen(!isOpen)}
            >
              {selectedValue ? selectedValue : title}
            </button>
            {isOpen ? (
              <ul
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton1"
              >
                {options.map((item: any, index: number) => {
                  return (
                    <li key={index} onClick={() => onItemClick(item.value)}>
                      <button className="dropdown-item">{item.label}</button>
                    </li>
                  );
                })}
              </ul>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}
