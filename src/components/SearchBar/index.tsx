import closeIcon from "../../assets/images/close.png";

export default function SearchBar({ onChange, value, onCrossIconClick }: any) {
  return (
    <div className="search">
      <input
        className="form-control"
        list="datalistOptions"
        id="exampleDataList"
        placeholder="Search by device ID"
        onChange={onChange}
        value={value}
      />
      {value && (
        <button
          className="search-content-remove"
          onClick={() => onCrossIconClick()}
        >
          <img src={closeIcon} alt="" />
        </button>
      )}
    </div>
  );
}
