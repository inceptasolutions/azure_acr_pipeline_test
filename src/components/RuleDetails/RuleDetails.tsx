import { useEffect, useState } from "react";
import { DropdownButton, Dropdown } from "react-bootstrap";
import deviceIcon from "../../assets/images/device-rules.png";
import removeIcon from "../../assets/images/remove.png";
import yesIcon from "../../assets/images/yes.png";
import editIcon from "../../assets/images/edit.png";
import backIcon from "../../assets/images/back-arrow.png";
import Table from "../../components/DeviceTable";
import api from "../../services/http";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import {
  Rule,
  RuleCreateRequest,
  RuleDeleteRequest,
  RuleForTable,
  Unit,
} from "../../helpers/types";

/**
 * @type {object}
 *
 * RuleDetailsProps is for React component props that come from React parent component
 */
type RuleDetailsProps = {
  rule: any[];
  goToSchedule: Function;
  deviceId: string;
  rulesComponents: string[];
  loadDeviceData: Function;
};

toast.configure();

const RuleDetails = ({
  rule,
  goToSchedule,
  deviceId,
  rulesComponents,
  loadDeviceData,
}: RuleDetailsProps) => {
  const unit: Unit = {
    Temperature: "°C",
    Pressure: "hPa",
    Humidity: "%",
    "Air Quality": "",
  };

  const unitScaleMax: Unit = {
    Temperature: "100",
    Pressure: "1100",
    Humidity: "100",
    "Air Quality": "350",
  };

  const ruleType: Unit = {
    Temperature: "condition.temperature",
    Humidity: "condition.humidity",
    Pressure: "condition.pressure",
    "Air Quality": "condition.airQuality",
  };

  const ruleComponentId: Unit = {
    Temperature: "temp",
    Humidity: "humi",
    Pressure: "pressure",
    "Air Quality": "aq",
  };

  const [data, setData] = useState<RuleForTable[]>([]);
  const allTypes = rulesComponents;
  const [newRule, setNewRule] = useState<boolean>(false);
  const [selectedType, setSelectedType] = useState<string>("");
  const [selectedDuration, setSelectedDuration] = useState<number>();
  const [minLimit, setMinLimit] = useState<any>(0);
  const [maxLimit, setMaxLimit] = useState<any>(0);
  const [outletStatus, setOutletStatus] = useState<boolean>(false);
  const [deviceRule, setDeviceRule] = useState<boolean>(false);
  const [loadSpinner, setLoadSpinner] = useState<boolean>(false);
  const [currentComponentTypeValue, setCurrentComponentTypeValue] =
    useState<RuleForTable>();

  const tableStyle = {
    border: "none",
    boxShadow: "none",
  };

  /**
   * Columns for rule table data that represents as table heading
   */
  const columns = [
    {
      Header: "Type",
      accessor: "type",
      width: 30,
    },
    {
      Header: "Min. Limit",
      accessor: "minLimit",
      width: 100,
    },
    {
      Header: "Max. Limit",
      accessor: "maxLimit",
      width: 110,
    },
    {
      Header: "Duration",
      accessor: "duration",
      Cell: (props: any) => <span>{props.value} minutes</span>,
      width: 110,
    },
    {
      Header: "Outlet Status",
      accessor: "outletStatus",
      Cell: (props: any) => {
        return handleRuleStatus(props);
      },
      width: 110,
    },
    {
      accessor: "edit",
      sortable: false,
      Cell: (props: any) => {
        return (
          <div className="d-flex align-items-center justify-content-end">
            <button className="close" onClick={() => handleEdit(props)}>
              <img src={editIcon} alt="icon" />
            </button>
          </div>
        );
      },
      width: 5,
    },
    {
      accessor: "delete",
      sortable: false,
      Cell: () => {
        return (
          <div className="d-flex align-items-center justify-content-start">
            <button className="close" onClick={() => handleDelete()}>
              <img src={removeIcon} alt="icon" />
            </button>
          </div>
        );
      },
      width: 5,
    },
  ];

  /**
   * @function handleMinMaxLimit
   * Set different type of symbols according the componentId
   * Set min or max limit value depending on conditionComponentDirection value
   * such as subseed or exceed or window
   *
   * @param {any} props
   * @param {string} str
   * @returns string as a min limit or max limit value
   */
  const handleMinMaxLimit = (props: any, str: string) => {
    const symbol: string =
      props[0]?.componentId === ruleComponentId.Temperature
        ? "°C"
        : props[0]?.componentId === ruleComponentId.Pressure
        ? "hPa"
        : props[0]?.componentId === ruleComponentId.Humidity
        ? "%"
        : "";

    for (let i = 0; i < props.length; i++) {
      if (props[i].conditionComponentDirection === "change.subseed") {
        if (str === "max") {
          return `${props[i].conditionComponentLimit}${symbol}`;
        }
      } else if (props[i].conditionComponentDirection === "change.exceed") {
        if (str === "min") {
          return `${props[i].conditionComponentLimit}${symbol}`;
        }
      } else if (props[i].conditionComponentDirection === "change.window") {
        if (str === "min") {
          return `${props[i].minLimit}${symbol}`;
        } else if (str === "max") {
          return `${props[i].maxLimit}${symbol}`;
        }
      }
    }
  };

  /**
   * @function sliceConditionComponentType
   * Format string of condition component type as it has 'condition.' word at first on the type string
   * that is to be removed
   *
   * @param {string} str
   * @returns string as a component type
   */
  const sliceConditionComponentType = (str: string) => {
    let type = str?.split(".")[1];
    type = type?.charAt(0).toUpperCase() + type?.slice(1);
    if (type === "AirQuality") {
      type = "Air Quality";
    }
    return type;
  };

  /**
   * @function handleRuleStatus
   * Handle outlet status of rule
   * depending on 'action.powerOn' and 'action.powerOff'
   *
   * @param {any} props
   * @returns {HTMLElement} HTML element within CSS changes in className
   */
  const handleRuleStatus = (props: any) => {
    if (props.value === "action.powerOn") {
      return <span className="status onn ml-3">ON</span>;
    } else {
      return <span className="status off">OFF</span>;
    }
  };

  /**
   * @function handleRuleDataForTable
   * Make rule data be ready for the data table.
   * Every column accessor name must be matched with property name.
   * Update data state that is inserted into data table
   *
   * @param {Array} rule - Array of interface Rule
   */
  const handleRuleDataForTable = (rule: Rule[]) => {
    const ruleData: RuleForTable[] = [
      {
        type: sliceConditionComponentType(rule[0]?.conditionComponentType),
        minLimit: handleMinMaxLimit(rule, "min"),
        maxLimit: handleMinMaxLimit(rule, "max"),
        duration: rule[0]?.conditionComponentDuration,
        outletStatus: rule[0]?.actionComponentType,
      },
    ];

    if (ruleData.length) setData(ruleData);
  };

  useEffect(() => {
    if (rule.length) {
      // Whenever rule state is updated, call handleRuleDataForTable
      handleRuleDataForTable(rule);
    }
  }, [rule]);

  useEffect(() => {
    // whenever rule is saved, render the component to update the table
  }, [deviceRule]);

  /**
   * @function handleEdit
   * Keep the rule info from table row
   * As there is only one rule, object is de-structured from props.data[0]
   * Update the states that is used in edit form data as values
   *
   * @param {any} props
   */
  const handleEdit = (props: any) => {
    setNewRule(true);

    const { type, duration, maxLimit, minLimit, outletStatus }: any =
      props.data[0];

    setSelectedType(type);
    setSelectedDuration(parseInt(duration));
    setMinLimit(minLimit?.replace(/(^\d+)(.+$)/i, "$1"));
    setMaxLimit(maxLimit?.replace(/(^\d+)(.+$)/i, "$1"));

    if (outletStatus === "action.powerOn") {
      setOutletStatus(true);
    } else {
      setOutletStatus(false);
    }

    setCurrentComponentTypeValue({
      type,
      duration,
      minLimit,
      maxLimit,
      outletStatus,
    });
  };

  /**
   * @function handleOutletStatus
   * Update outletStatus as checked value from UI input in the rule edit mode
   *
   * @param {any} e
   */
  const handleOutletStatus = (e: any) => {
    setOutletStatus(e.target.checked);
  };

  /**
   * @function onMinChange
   * Update minLimit as input value from UI in the rule edit mode
   *
   * @param {any} e
   */
  const onMinChange = (e: any) => {
    setMinLimit(e.target.value);
  };

  /**
   * @function onMaxChange
   * Update maxLimit as input value from UI in the rule edit mode
   *
   * @param {any} e
   */
  const onMaxChange = (e: any) => {
    setMaxLimit(e.target.value);
  };

  /**
   * @function handleSelectedType
   * Update selectedType as dropdown item input value from UI in the rule edit mode
   * selectedType is used in dropdown button title
   *
   * @param {string} type
   */
  const handleSelectedType = (type: string) => {
    setSelectedType(type);
  };

  /**
   * @function handleSelectedDuration
   * Update selectedDuration as dropdown item input value from UI in the rule edit mode
   * selectedDuration is used in dropdown button title
   *
   * @param {number} duration
   */
  const handleSelectedDuration = (duration: number) => {
    setSelectedDuration(duration);
  };

  /**
   * @function handleDelete
   * Make deleteRequest variable according to the interface RuleDeleteRequest
   * Values are to be null
   * Call the function sendDataToRuleAPI to delete the rule through API
   */
  const handleDelete = () => {
    setLoadSpinner(true);

    const deleteRequest: RuleDeleteRequest = {
      deviceId: deviceId,
      ruleName: "delete_rule",
      condition: {
        type: "null",
        componentId: "null",
        min: "null",
        max: "null",
        duration: "null",
      },
      notifyEvent: false,
      action: {
        type: "null",
      },
    };

    sendDataToRuleAPI(deleteRequest);
  };

  /**
   * @function addNewRule
   * Handle if any required info is missing, returns
   * Otherwise, create params for sending request body according to interface RuleCreateRequest
   * Call the function sendDataToRuleAPI to add new rule through the rules API
   *
   * @returns
   */
  const addNewRule = () => {
    if (!selectedType) {
      toast.error("Please select a type");
      return;
    }
    if (!selectedDuration) {
      toast.error("Please select a duration");
      return;
    }
    if (Number(minLimit) > Number(maxLimit)) {
      toast.error("Minimum limit can not be greater than the maximum limit");
      return;
    }
    const params: RuleCreateRequest = {
      deviceId: deviceId,
      ruleName: "Rule 1",
      condition: {
        type: ruleType[selectedType],
        componentId: ruleComponentId[selectedType],
        min: minLimit,
        max: maxLimit,
        limit: "10",
        duration: selectedDuration ? selectedDuration.toString() : "",
      },
      notifyEvent: true,
      action: {
        type: outletStatus ? "powerOn" : "powerOff",
      },
    };

    sendDataToRuleAPI(params);
  };

  /**
   * @function sendDataToRuleAPI
   * Call the rules API with param requestBody.
   * Update the necessary states
   * setTimeout is used for toast, loading spinner because it takes time to response in backend
   *
   * @param {any} requestBody
   */
  const sendDataToRuleAPI = (requestBody: any) => {
    api<RuleCreateRequest>("post", "rules", "", requestBody)
      .then((data) => {
        setNewRule(false);
        setSelectedType("");
        setSelectedDuration(undefined);
        setMinLimit(0);
        setMaxLimit(0);
        setOutletStatus(false);
        setLoadSpinner(true);
        setData([]);

        setTimeout(() => {
          toast.success("Rule updated successfully.");
          setTimeout(() => {
            loadDeviceData();
            setDeviceRule(true);
            setLoadSpinner(false);
          }, 3000);
        }, 5000);
      })
      .catch((error) => {});
  };

  /**
   * @function rangeSlider
   * Function to change the CSS depending on unit scale values updating
   */
  const rangeSlider = () => {
    // First let's set the colors of our sliders
    const settings = {
      fill: "#DA291C",
      background: "#F8F9FC",
      border: "1px solid #F8F9FC",
    };

    // First find all our sliders
    const sliders = document.querySelectorAll(".range-slider");

    // Iterate through that list of sliders
    // ... this call goes through our array of sliders [slider1,slider2,slider3] and inserts them one-by-one into the code block below with the variable name (slider). We can then access each of them by calling slider
    Array.prototype.forEach.call(sliders, (slider) => {
      // Look inside our slider for our input add an event listener
      //   ... the input inside addEventListener() is looking for the input action, we could change it to something like change
      slider.querySelector("input").addEventListener("input", (event: any) => {
        // 1. apply our value to the span
        slider.querySelector("span").innerHTML = event.target.value;
        // 2. apply our fill to the input
        applyFill(event.target, settings);
      });
      // Don't wait for the listener, apply it now!
      applyFill(slider.querySelector("input"), settings);
    });
  };

  /**
   * @function applyFill
   * This function applies the fill to our sliders by using a linear gradient background
   *
   * @param {any} slider
   * @param {any} settings
   */
  const applyFill = (slider: any, settings: any) => {
    // Let's turn our value into a percentage to figure out how far it is in between the min and max of our input
    const percentage =
      (100 * (slider.value - slider.min)) / (slider.max - slider.min);
    // now we'll create a linear gradient that separates at the above point
    // Our background color will change here
    const bg = `linear-gradient(90deg, ${settings.fill} ${percentage}%, ${
      settings.background
    } ${percentage + 0.1}%)`;
    slider.style.background = bg;
  };

  useEffect(() => {
    // Whenever minLimit or maxLimit state is updated, call the rangeSlider function
    rangeSlider();
  }, [minLimit, maxLimit]);

  /**
   * @function setDefaultValue
   * Function to set default value when type is changed from dropdown button in rule edit mode.
   * If current component type is matched, required info of the rule are assigned in the states.
   * Otherwise, all states are to be empty or zero
   */
  const setDefaultValue = () => {
    if (currentComponentTypeValue?.type === selectedType) {
      if (selectedType === "Air Quality") {
        setMinLimit(currentComponentTypeValue?.minLimit);
        setMaxLimit(currentComponentTypeValue?.maxLimit);
      } else {
        setMinLimit(
          currentComponentTypeValue?.minLimit?.replace(/(^\d+)(.+$)/i, "$1")
        );
        setMaxLimit(
          currentComponentTypeValue?.maxLimit?.replace(/(^\d+)(.+$)/i, "$1")
        );
      }

      setSelectedDuration(parseInt(currentComponentTypeValue?.duration));

      if (currentComponentTypeValue?.outletStatus === "action.powerOn") {
        setOutletStatus(true);
      } else {
        setOutletStatus(false);
      }
    } else {
      setMinLimit(0);
      setMaxLimit(0);
      setSelectedDuration(undefined);
      setOutletStatus(false);
    }
  };

  useEffect(() => {
    // Whenever selectedType is updated setDefaultValue function is called
    setDefaultValue();
  }, [selectedType]);

  /**
   * @function clearAllData
   * Function to set default value when user clicks the back button from UI in rule edit mode
   */
  const clearAllData = () => {
    setSelectedDuration(undefined);
    setSelectedType("");
    setMinLimit(0);
    setMaxLimit(0);
    setOutletStatus(false);
    setNewRule(false);
  };

  useEffect(() => {
    if (data.length) {
      setLoadSpinner(false);
    }
  }, [data]);

  return (
    <>
      <div className="device-schedule">
        {/* Device Rule title area starts*/}
        <div className="title-area">
          <h4>
            <img src={deviceIcon} alt="icon" /> Device Rules
          </h4>
          <div className="d-flex">
            <button
              className="btn btn-red shadow-none me-3"
              onClick={() => goToSchedule()}
            >
              {" "}
              Go to Schedule
            </button>

            {/* When a device has no rule, Add New Rule button is rendered */}
            {!(rule.length > 0) && (
              <button
                className="btn btn-red shadow-none"
                onClick={() => setNewRule(true)}
                disabled={loadSpinner}
              >
                {" "}
                Add New Rule
              </button>
            )}
          </div>
        </div>
        {/* Device Rule title area ends*/}

        <p>
          Device rules will change the outlet status to ON or OFF when certain
          conditions are met. A condition must be met for the wait interval
          before it is applied.
        </p>

        {loadSpinner && (
          <div className="loader-sm">
            <Loader type="Circles" color="#DA291C" height={100} width={100} />
          </div>
        )}

        {/* When rule data is available, Table component is rendered */}
        {data && data.length > 0 && !loadSpinner && (
          <Table
            style={tableStyle}
            columns={columns}
            data={data}
            showPagination={false}
            columnsFilter={false}
          />
        )}

        {newRule && (
          <p className="pb-3 pt-0">
            When limits are met for duration, the rule will be applied.
          </p>
        )}

        {/* This section will be shown when add new rule or edit a rule mode is active */}
        {newRule && (
          <>
            <div className="container-fluid ps-2">
              <div className="row">
                <div className="col-md-12 dropdown-buttons">
                  <div className="row">
                    {/* Rule type starts */}
                    <div className="col-md-6 ps-1 pe-3">
                      <div className="dropdown">
                        <DropdownButton
                          variant="light"
                          title={!selectedType ? "Type" : selectedType}
                        >
                          {allTypes.map((type, idx) => (
                            <Dropdown.Item
                              key={idx}
                              onClick={() => handleSelectedType(type)}
                            >
                              {" "}
                              {type}{" "}
                            </Dropdown.Item>
                          ))}
                        </DropdownButton>
                      </div>
                    </div>
                    {/* Rule type ends */}

                    {/* Duration part starts */}
                    <div className="col-md-6 ps-3 pe-1">
                      <div className="dropdown rule-duration">
                        <DropdownButton
                          variant="light"
                          title={
                            !selectedDuration
                              ? "Duration"
                              : `${selectedDuration} minutes`
                          }
                        >
                          {Array.from(Array(15).keys()).map((entry, idx) => (
                            <Dropdown.Item
                              key={idx}
                              onClick={() => handleSelectedDuration(entry + 1)}
                            >
                              {" "}
                              {`${entry + 1} minutes`}{" "}
                            </Dropdown.Item>
                          ))}
                        </DropdownButton>
                      </div>
                    </div>
                    {/* Duration part ends */}
                  </div>
                </div>
              </div>
            </div>
            <div className="container-fluid ps-2">
              <div className="row">
                {/* Min Limit part starts */}
                <div className="col-md-6 ps-1 pe-3">
                  <div className="range-slider">
                    <p>
                      Min. Limit{" "}
                      <span className="range-slider__value">{`${minLimit}${
                        selectedType && unit[selectedType]
                          ? unit[selectedType]
                          : ""
                      }`}</span>
                    </p>
                    <input
                      className="range-slider__range"
                      type="range"
                      value={minLimit}
                      min="0"
                      max={`${
                        selectedType && unitScaleMax[selectedType]
                          ? unitScaleMax[selectedType]
                          : "100"
                      }`}
                      onChange={onMinChange}
                    />
                  </div>
                </div>
                {/* Min Limit part ends */}

                {/* Max Limit part starts */}
                <div className="col-md-6 ps-3 pe-1">
                  <div className="range-slider">
                    <p>
                      Max. Limit{" "}
                      <span className="range-slider__value">{`${maxLimit}${
                        selectedType && unit[selectedType]
                          ? unit[selectedType]
                          : ""
                      }`}</span>
                    </p>
                    <input
                      className="range-slider__range"
                      type="range"
                      value={maxLimit}
                      min="0"
                      max={`${
                        selectedType && unitScaleMax[selectedType]
                          ? unitScaleMax[selectedType]
                          : "100"
                      }`}
                      onChange={onMaxChange}
                    />
                  </div>
                </div>
                {/* Max Limit part ends */}
              </div>
              <div className="row slider-buttons">
                <div className="col-12">
                  <div className="title-area">
                    <div className="mt-5 d-flex align-items-center justify-content-end w-100">
                      {/* Outlet status toggle part starts */}
                      <div className="form-check form-switch">
                        <label className="form-check-label me-5">
                          Set Outlet Status OFF
                        </label>
                        <input
                          className="form-check-input float-none"
                          type="checkbox"
                          id="flexSwitchCheckDefault2"
                          checked={outletStatus}
                          onChange={handleOutletStatus}
                        />
                        <label className="form-check-label ms-2">ON</label>
                      </div>
                      {/* Outlet status toggle part ends */}

                      {/* Back button and save button starts */}
                      <div className="d-flex ps-5">
                        <button className="close me-2" onClick={clearAllData}>
                          <img
                            style={{ width: "20px" }}
                            src={backIcon}
                            alt="icon"
                          />
                        </button>
                        <button className="yes" onClick={addNewRule}>
                          <img src={yesIcon} alt="icon" />
                        </button>
                      </div>
                      {/* Back button and save button ends */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default RuleDetails;
