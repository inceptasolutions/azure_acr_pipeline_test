import { useEffect, useState } from "react";
import api from "../../services/http";
import settingsIcon from "../../assets/images/settings.png";
import { toast } from "react-toastify";
import Loader from "react-loader-spinner";
import { LastReadingResponse } from "../../helpers/types";

/**
 * @type {object}
 *
 * ManualOverrideProps is for React component props that come from React parent component
 */
type ManualOverrideProps = {
  device: LastReadingResponse | undefined;
  getLastReading: Function;
  setLoaderForOverride: Function;
};

/**
 * @type {object}
 *
 * ManualOverrideParam is for request body that is sent to the deviceOverride API
 */
type ManualOverrideParam = {
  deviceId: any;
  set: string;
};

toast.configure();

const ManualOverride = ({
  device,
  getLastReading,
  setLoaderForOverride,
}: ManualOverrideProps) => {
  /**
   * @function loadOverrideStatus
   * Check the device's function array to get the outlet status value
   *
   * @returns {boolean} temporary override status as on/off
   */
  const loadOverrideStatus = () => {
    if (device?.function) {
      const funcArray = device.function;

      for (let i = 0; i < funcArray.length; i++) {
        if (
          funcArray[i]["componentId"] === "0" &&
          funcArray[i]["componentName"] === "toggle"
        ) {
          if (funcArray[i]["componentValue"] === "on") {
            return true;
          } else {
            return false;
          }
        }
      }
    }
    return false;
  };

  const [override, setOverride] = useState<boolean>(loadOverrideStatus());
  const [loadSpinner, setLoadSpinner] = useState(false);

  /**
   * @function handleOverride
   * Call the deviceOverride API with params data.
   * If successful, getLastReading is called to get updated data with status.
   * setTimeout is used because it takes time to update lastReading data in backend.
   * Handle loadSpinner
   *
   * @param {any} e - checked value from input field
   */
  const handleOverride = (e: any) => {
    const value = e.target.checked;

    const params: ManualOverrideParam = {
      deviceId: device?.deviceId,
      set: value ? "on" : "off",
    };

    setLoadSpinner(true);
    setLoaderForOverride(true);

    api("post", "deviceOverride", "", params)
      .then((data) => {
        setOverride(value);

        setTimeout(() => {
          getLastReading();
          toast.success("Temporary override successful");
          setTimeout(() => {
            getLastReading();
            setTimeout(() => {
              setLoaderForOverride(false);
              setLoadSpinner(false);
            }, 2000);
          }, 3000);
        }, 5000);
      })
      .catch((error) => {
        setLoadSpinner(false);
        toast.error("Sorry, Temporary override not successful");
      });
  };

  useEffect(() => {
    // Whenever device is updated, override state is updated to view in the UI
    if (loadOverrideStatus()) {
      setOverride(true);
    } else {
      setOverride(false);
    }
  }, [device]);

  return (
    <>
      {/* Temporary override area starts */}
      <div className="device-schedule">
        <div className="title-area">
          <h4>
            <img src={settingsIcon} alt="icon" /> Temporary Override
          </h4>
          <div className="d-flex">
            <div className="form-check form-switch">
              <label className="form-check-label me-5">OFF</label>
              <input
                className="form-check-input float-none"
                type="checkbox"
                id="flexSwitchCheckDefault1"
                checked={override}
                onChange={handleOverride}
                disabled={loadSpinner}
              />
              <label className="form-check-label ms-2">ON</label>
            </div>
          </div>
        </div>
        {loadSpinner && (
          <div className="loader-sm" style={{ height: "60px" }}>
            <Loader type="Circles" color="#DA291C" height={100} width={100} />
          </div>
        )}
        {!loadSpinner && (
          <p>
            Override the device schedule and rules to change the outlet status.
            Schedule and Rules are inactive when override is active.
          </p>
        )}
      </div>
      {/* Temporary override area ends */}
    </>
  );
};

export default ManualOverride;
