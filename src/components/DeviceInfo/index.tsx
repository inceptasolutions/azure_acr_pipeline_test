/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import deviceIdImage from "../../assets/images/id.png";
import outletImage from "../../assets/images/outlet.png";
import locationImage from "../../assets/images/washa.png";
import lastReadingIcon from "../../assets/images/last-reading.png";
import updateIcon from "../../assets/images/update.png";
import temperatureIcon from "../../assets/images/temperature.png";
import humidityIcon from "../../assets/images/humidity-above-max.png";
import airQualityIcon from "../../assets/images/air-quality-above-max.png";
import pressureIcon from "../../assets/images/pressure-above-max.png";
import outletStatusIcon from "../../assets/images/onn.png";
import clockIcon from "../../assets/images/clock-red.png";
import Loader from "react-loader-spinner";
import { Components, LastReadingResponse } from "../../helpers/types";

/**
 * Interface is for React component props that comes from React parent component
 *
 * @interface DeviceInfoProps
 */
interface DeviceInfoProps {
  device: LastReadingResponse | undefined;
  getLastReading: Function;
  time: string;
  loaderForOverride: boolean;
}



export default function DeviceInfo({
  device,
  getLastReading,
  time,
  loaderForOverride,
}: DeviceInfoProps) {
  const [temperature, setTemperature] = useState("");
  const [humidity, setHumidity] = useState("");
  const [airQuality, setAirQuality] = useState("");
  const [pressure, setPressure] = useState("");

  /**
   * @function formatHostname
   * Format device type name to remove the 'host.' word from every device type string
   *
   * @param {string} type - device type
   * @returns {string} type that represents device type on top of the device details page
   */
  const formatHostname = (type: string = "") => {
    if (type) {
      const str = type.split(".")[1];
      return str.charAt(0).toUpperCase() + str.slice(1);
    }
    return "";
  };

  useEffect(() => {
    setComponents();

    setTimeout(() => {
      handleOutletStatus();
    }, 5000);
  }, [device]);

  /**
   * @function setComponents
   * Update states of four component values matching with components name.
   * Only those component values will be shown in the UI through JSX
   */
  const setComponents = () => {
    device?.function.forEach((entry) => {
      if (entry.componentName === Components.Temperature) {
        setTemperature(entry.componentValue);
      }
      if (entry.componentName === Components.Humidity) {
        setHumidity(entry.componentValue);
      }
      if (entry.componentName === Components.Pressure) {
        setPressure(entry.componentValue);
      }
      if (entry.componentName === Components.AirQuality) {
        setAirQuality(entry.componentValue);
      }
    });
  };

  /**
   * @function handleOutletStatus
   * Check the device's function array to find the outlet status
   *
   * @returns {string} outlet status as On/Off
   */
  const handleOutletStatus = () => {
    let status: string = "";

    if (device?.function) {
      const funcArray = device.function;

      for (let i = 0; i < funcArray.length; i++) {
        if (
          funcArray[i]["componentId"] === "0" &&
          funcArray[i]["componentName"] === "toggle"
        ) {
          status = funcArray[i]["componentValue"] === "on" ? "On" : "Off";
        }
      }
    }

    return status;
  };

  return (
    <>
      {/* First row with id type Location starts*/}
      <div className="container-fluid p-0 pt-4">
        <div className="row">
          <div className="col-lg-6 col-xl-4 mb-3 mb-xl-0">
            <div className="item-box">
              <div className="icon-wrapper">
                <img src={deviceIdImage} alt="icon" />
              </div>
              <div className="item-info">
                <p>Device ID</p>
                <h3>{device?.deviceId}</h3>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xl-4 mb-3 mb-xl-0">
            <div className="item-box">
              <div className="icon-wrapper">
                <img src={outletImage} alt="icon" />
              </div>
              <div className="item-info">
                <p>Type</p>
                <h3>{formatHostname(device ? device?.type : "")}</h3>
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-xl-4">
            <div className="item-box">
              <div className="icon-wrapper">
                <img src={locationImage} alt="icon" />
              </div>
              <div className="item-info">
                <p>Location</p>
                <h3>{device?.location}</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* First row with id type Location ends*/}

      {/* Second row with last reading starts */}
      <div className="last-reading-box">
        <div className="title-area">
          <h4>
            <img src={lastReadingIcon} alt="icon" /> Last Reading
          </h4>
          <div className="d-flex align-items-center">
            <span className="me-3 d-flex align-items-center">
              <img src={clockIcon} alt="icon" className="me-1" /> {time}
            </span>
            <button
              className="btn btn-red-bordered shadow-none"
              onClick={() => getLastReading()}
            >
              <img src={updateIcon} alt="icon" /> Get New Reading
            </button>
          </div>
        </div>

        {loaderForOverride && (
          <div className="loader-sm" style={{ height: "60px" }}>
            <Loader type="Circles" color="#DA291C" height={100} width={100} />
          </div>
        )}

        {!loaderForOverride && (
          <div className="weather-content">
            <div className="container mr-3 p-0">
              <div className="row g-0">
                {temperature && (
                  <div className="col-xl-6 col-xxl-3">
                    <div className="item-box">
                      <div className="icon-wrapper">
                        <img src={temperatureIcon} alt="icon" />
                      </div>
                      <div className="item-info">
                        <p>Temperature</p>
                        <h3>{parseFloat(temperature).toFixed(1)}°C</h3>
                      </div>
                    </div>
                  </div>
                )}

                {humidity && (
                  <div className="col-xl-6 col-xxl-3">
                    <div className="item-box">
                      <div className="icon-wrapper">
                        <img src={humidityIcon} alt="icon" />
                      </div>
                      <div className="item-info">
                        <p>Humidity</p>
                        <h3>{parseFloat(humidity).toFixed(1)}%</h3>
                      </div>
                    </div>
                  </div>
                )}

                {airQuality && (
                  <div className="col-xl-6 col-xxl-3">
                    <div className="item-box">
                      <div className="icon-wrapper">
                        <img src={airQualityIcon} alt="icon" />
                      </div>
                      <div className="item-info">
                        <p>Air Quality</p>
                        <h3>{parseFloat(airQuality).toFixed(1)}</h3>
                      </div>
                    </div>
                  </div>
                )}

                {pressure && (
                  <div className="col-xl-6 col-xxl-3">
                    <div className="item-box">
                      <div className="icon-wrapper">
                        <img src={pressureIcon} alt="icon" />
                      </div>
                      <div className="item-info">
                        <p>Pressure</p>
                        <h3>{parseFloat(pressure).toFixed(1)} hPa</h3>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div className="outlet-status">
              <div className="item-box">
                <div className="icon-wrapper">
                  <img src={outletStatusIcon} alt="icon" />
                </div>
                <div className="item-info">
                  <p>Outlet Status</p>
                  <h3>{handleOutletStatus()}</h3>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
      {/* Second row with last reading ends*/}
    </>
  );
}
