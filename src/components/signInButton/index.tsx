import React, { useState } from 'react';
import { useMsal } from "@azure/msal-react";
import { loginRequest } from "../../authConfig";

import { IPublicClientApplication } from "@azure/msal-browser";


export const SignInButton = () => {
   
    const { instance } = useMsal();
    
    function handleLogin(instance: IPublicClientApplication) {
      instance.loginRedirect(loginRequest).catch(e => {
          console.error(e);
      });
    }

   
    return (
        <button className="signInButton" onClick={() => handleLogin(instance)}>Sign In</button>
    );
}