# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

# SmartConnect

This is a SmartConnect project by Incepta Solutions. It helps to make a decision the device be on or off based on some conditions as unit scale of humidity, temperature, pressure or air quality while subseeding or exceeding and to track the device that the device admin will get alarm notification. Moreover, it shows the current reading data of a device with device ID. User can add rule or schedule for a device.

## Features
- Device list
- Device details as last reading data
- Device rules (create, update and delete)
- Device schedule (create and update)
- Alarm notification data

# Project Folder Structure

> Folder structure options and naming conventions for this project

### A typical top-level directory layout

    .
    ├── build                   # Compiled files (alternatively `dist`)
    ├── public                  # Main html file
    ├── src                     # Source files (alternatively `lib` or `app`)
    ├── .env.example            # Env file (for example)
    ├── package.json            # Dependencies, scripts and identification the entry point
    ├── LICENSE
    └── README.md

> Use short lowercase names at least for the top-level folders except
> `LICENSE`, `README.md`

## How it works

The file structure maps directly to the route hierarchy, which maps directly to the UI hierarchy.

If we consider all folders being either a "generic" or a "feature" folder, we
only have one "feature" folder named pages but many "generic" folders.

Examples of folders under "pages" feature :

- Home
- DeviceDetails
- Admin

Examples of "generic" folders:

- assets
- components
- helpers
- routes
- services

Given this route config that is used in the root App.tsx file:

```js
const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/device-details/:id" component={DeviceDetails} />
      <Route exact path="/admin" component={Admin} />

      {/* Redirect to Home while any wrong pathname is tried */}
      <Route path="*" render={() => <Redirect to="/" />} />
    </Switch>
  );
};
```

And App.tsx file is as follows:

```js
function App() {
  return (
    <div className="App">
      <Router>
        {/* Common Header section for all pages */}
        <Header />
        <div className="main-wrapper">
          {/* Common Sidebar section for all pages */}
          <Sidebar />
          {/* All the routes are defined here like /home, /device-details, /admin etc.*/}
          <AppRoutes />
        </div>
      </Router>
    </div>
  );
}
```
The project directories are like below:

```
app
├── assets
│   └── images
├── components
│   └── DeviceInfo
│   |   ├── index.tsx
│   └── DeviceTable
│   |   ├── ColumnFilter.tsx
│   |   ├── index.tsx
│   └── ManualOverride
│   |   ├── index.tsx
│   └── RuleDetails
│   |   ├── RuleDetails.tsx
│   └── ScheduleDetails
│   |   ├── ScheduleDetails.tsx
│   └── ScheduleRule
│   |   ├── index.tsx
│   └── SearchBar
│   |   ├── index.tsx
│   └── Shared
│       └── Header
│       |   ├── index.tsx
│       └── Sidebar
│           └── index.tsx
├── helpers
│   └── types.ts
├── pages
│   └── Admin
│   |   ├── index.tsx
│   └── DeviceDetails
│   |   ├── index.tsx
│   └── Home
│       └── index.tsx
├── routes
│   └── index.tsx
├── services
│   └── http.ts
│
├── App.tsx
└── index.tsx
```

With this structure, when a route is selected, the related page is loaded and other components are rendered from the parent component on pages.

Try run this project
------------

In the project directory, you can run command:

### `npm install`

Install the essential npm packages

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

To deploy this project
------------
### `npm run build`

This will create a folder named build inside the root directory.

The build folder contains the static assets which will be deployed on the server.

## Technology Stack:

- React
- Bootstrap
- Ant Design
- TypeScript
- React-router-dom
- Axios
- React-toastify
- React-table
- React-loader-spinner